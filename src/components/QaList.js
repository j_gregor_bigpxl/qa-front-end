import { useEffect } from "react";
import { useState } from "react/cjs/react.development";
import ListFunction from "./ListFunction";

export default function QaList({ h1Data, altData }) {
  const totalH1s = h1Data.totalH1s;
  const totalAlts = altData.totalAlts;
  const [missingAlts, setMissingAlts] = useState([]);

  useEffect(() => {
    // console.log(altData.totalEmpty);
    for (let i = 0; i < altData.totalEmpty; i++) {
      setMissingAlts((missingAlts) => [...missingAlts, altData.missingTags[i]]);
    }
    // for (let i = 0; i < totalH1s; i++) {
    //   console.log(`Heading Text-${i + 1}: `, h1Data["h1#" + i]);
    // }
  }, [altData]);

  return (
    <div>
      <hr className="mt-5" />
      <ul>
        <li>Total H1's: {totalH1s ? totalH1s : 0}</li>
        <li>Total Imgs: {totalAlts ? totalAlts : 0}</li>
        <li>Missing Alt Tags: {altData.totalEmpty ? altData.totalEmpty : 0}</li>
        {altData.totalEmpty < 1
          ? null
          : missingAlts.map((u) => {
              return <ListFunction u={u} />;
            })}
      </ul>
    </div>
  );
}
