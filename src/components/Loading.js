import { MutatingDots } from "react-loader-spinner";

export default function Loading() {
  return (
    <MutatingDots
      radius="13"
      width="125"
      height="125"
      color="#FCE042"
      secondaryColor="#87F9A8"
    />
  );
}
