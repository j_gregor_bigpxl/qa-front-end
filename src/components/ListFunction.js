export default function ListFunction({ u }) {
  return (
    <li className="truncate w-auto text-center">
      Missing Alt Link:{" "}
      <a href={u} target="_blank" className="text-blue-400">
        {u}
      </a>
    </li>
  );
}
