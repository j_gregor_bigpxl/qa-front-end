import logo from "./logo.png";
import "./App.css";
import axios from "axios";
import React, { useState } from "react";
import Loading from "./components/Loading";
import QaList from "./components/QaList";

function App() {
  const [urlInput, setUrlInput] = useState("");
  const [loading, setLoading] = useState(false);
  const [h1Data, setH1data] = useState("");
  const [altData, setAltData] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    loadingScroll(true);
    urlInput.startsWith("http")
      ? axiosPost(urlInput)
      : axiosPost("http://" + urlInput);
  };

  const loadingScroll = (value) => {
    setLoading(value);
    if (loading)
      window.scroll({
        top: 100,
        left: 100,
        behavior: "auto",
      });
  };
  const axiosPost = (input) => {
    axios
      .get("http://localhost:80/", { params: { url: input } })
      .then((response) => {
        // response.data contains 2 objects. h1Data: {} & altTags: {}
        const data = response.data;
        const totalH1s = data.h1Data.totalH1s;
        setH1data(data.h1Data);
        setAltData(data.altTags);
        const emptyAlts = data.altTags.totalEmpty;

        // ------------ H1s --------------
        // console.log("Total H1's:", totalH1s);
        // If there are multiple h1 tags, this will print each of the h1's text
        // for (let i = 0; i < totalH1s; i++) {
        //   console.log(`Heading Text-${i + 1}: `, data.h1Data["h1#" + i]);
        // }
        // ------------ Alt Tags -----------
        // console.log("Total imgs:", data.altTags.totalAlts);
        // console.log("Total missing tags:", emptyAlts);
        // Prints link to each missing alt tag's src
        // for (let i = 0; i < emptyAlts; i++) {
        //   console.log(
        //     `Missing alt tag image ${i + 1}:`,
        //     data.altTags.missingTags[i]
        //   );
        // }
      })
      .then(() => setLoading(false))
      .catch((err) => {
        console.error(err);
        setLoading(false);
      });
  };

  const updateInput = (event) => {
    setUrlInput(event.target.value);
  };

  return (
    <div className="App">
      <img src={logo} className="App-logo block mb-5" alt="logo" />
      <p>Paste in the URL of the site you want to crawl</p>

      <form
        className="flex grid grid-rows-2 items-center justify-center"
        onSubmit={handleSubmit}
      >
        <input
          className="mt-5 rounded-md shadow-sm text-gray-800 text-base p-2 a"
          type="text"
          value={urlInput}
          onChange={updateInput}
        />

        <input
          type="submit"
          value="Submit"
          className="mt-4  bg-white hover:bg-gray-100 text-gray-800 font-semibold py-1 px-4 border text-base rounded shadow"
        />
      </form>

      {loading ? (
        <Loading />
      ) : h1Data ? (
        <QaList h1Data={h1Data} altData={altData} />
      ) : null}
    </div>
  );
}

export default App;
